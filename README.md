> ⚠ Please note:  
> This README was retroactively created from the
> [original forum post](http://forum.sa-mp.com/showthread.php?t=325273)
> and was modified slightly to remove stuff like weird typos and expired
> links.

**<span style="color: red">ATTENTION</span>**: This Include only works
with SA-MP 0.3e RC2 and higher!

# OnPlayerCamera...
> v0.1a

Hello guys!  
This is just some callback-include I made after I read this.

With this include you can call `OnPlayerCameraMoved(playerid)` and
`OnPlayerCameraLookMoved(playerid)` after the movement of a player's
position or `LookAtPosition` has been done (`InterpolateCameraPos` and
`InterpolateCameraLookAt`).
