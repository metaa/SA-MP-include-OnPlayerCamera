/*=========================================================*\
|                                                           |
|   OnPlayerCameraMoved & OnPlayerCameraLookMoved by Meta   |
|                                                           |
\*=========================================================*/

forward OnPlayerCameraMoved(playerid, Float:FromX, Float:FromY, Float:FromZ, Float:ToX, Float:ToY, Float:ToZ, time, cut);
stock OnPlayerCameraMoved_SYS(playerid, Float:FromX, Float:FromY, Float:FromZ, Float:ToX, Float:ToY, Float:ToZ, time, cut)
{
	DeletePVar(playerid, "InterpolateCameraPosTimer");
	if(funcidx("OnPlayerCameraMoved_SYS") != -1)
	{
	    CallLocalFunction("OnPlayerCameraMoved", "dffffffdd", playerid, FromX, FromY, FromZ, ToX, ToY, ToZ, time, cut);
	}
}

#if defined _ALS_OnPlayerCameraMoved_SYS
	#undef OnPlayerCameraMoved_SYS
#else
	#define _ALS_OnPlayerCameraMoved_SYS
#endif
#define OnPlayerCameraMoved_SYS OnPlayerCameraMoved

forward OnPlayerCameraLookMoved(playerid, Float:FromX, Float:FromY, Float:FromZ, Float:ToX, Float:ToY, Float:ToZ, time, cut);
stock OnPlayerCameraLookMoved_SYS(playerid, Float:FromX, Float:FromY, Float:FromZ, Float:ToX, Float:ToY, Float:ToZ, time, cut)
{
	DeletePVar(playerid, "InterpolateCameraLookAtTimer");
	if(funcidx("OnPlayerCameraMoved_SYS") != -1)
	{
	    CallLocalFunction("OnPlayerCameraLookMoved", "dffffffdd", playerid, FromX, FromY, FromZ, ToX, ToY, ToZ, time, cut);
	}
}

stock InterpolateCameraPosEx(playerid, Float:FromX, Float:FromY, Float:FromZ, Float:ToX, Float:ToY, Float:ToZ, time, cut = CAMERA_CUT)
{
	if(GetPVarType(playerid, "InterpolateCameraPosTimer"))
	{
		KillTimer(GetPVarInt(playerid, "InterpolateCameraPosTimer"));
	}
    InterpolateCameraPos(playerid, FromX, FromY, FromZ, ToX, ToY, ToZ, time, cut);
    SetPVarInt(playerid, "InterpolateCameraPosTimer", SetTimerEx("OnPlayerCameraMoved", time, 0, "dffffffdd", playerid, FromX, FromY, FromZ, ToX, ToY, ToZ, time, cut));
}

#if defined _ALS_InterpolateCameraPos
	#undef InterpolateCameraPos
#else
	#define _ALS_InterpolateCameraPos
#endif
#define InterpolateCameraPos InterpolateCameraPosEx

stock InterpolateCameraLookAtEx(playerid, Float:FromX, Float:FromY, Float:FromZ, Float:ToX, Float:ToY, Float:ToZ, time, cut = CAMERA_CUT)
{
	if(GetPVarType(playerid, "InterpolateCameraLookAtTimer"))
	{
		KillTimer(GetPVarInt(playerid, "InterpolateCameraLookAtTimer"));
	}
	InterpolateCameraLookAt(playerid, FromX, FromY, FromZ, ToX, ToY, ToZ, time, cut);
    SetPVarInt(playerid, "InterpolateCameraLookAtTimer", SetTimerEx("OnPlayerCameraLookMoved", time, 0, "dffffffdd", playerid, FromX, FromY, FromZ, ToX, ToY, ToZ, time, cut));
}

#if defined _ALS_InterpolateCameraLookAt
	#undef InterpolateCameraLookAt
#else
	#define _ALS_InterpolateCameraLookAt
#endif
#define InterpolateCameraLookAt InterpolateCameraLookAtEx
